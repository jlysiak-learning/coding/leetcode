// A - array of bulbs. A[i] its position in row.
// return number of moments where all turned on bulbs are shined
// start from 0 to length-1, switch on bulbs ( A[i] represents a bulb's position)
// A[i] bulb shined if: 1) A[i] is switched 2) 1..A[i]-1 all are shined
// examples:
// input: {1,2,3,4,5} output: 5
// input: {1} output: 1
// input: {2,3,4,1,5} output: 2
// input: {2,1,3,5,4} output: 3

#include <iostream>
#include <vector>
#include <cassert>

/**
 * Count moments when lights are turning on
*/
int countMoments( std::vector< int > const & seq ) {
  auto bulbs{ std::vector< bool >( seq.size(), false ) }; // state of bulbs
  auto idx{ 0u }; // how far we are in bulbs chain from the beginning, bulbs below idx are turned on
  auto cnt{ 0u }; // how many times we switched bulbs
  for ( auto bulb : seq ) {
    std::cout << "Switch bulb: " << bulb << std::endl;
    bulbs[ bulb ] = true;
    if ( idx < seq.size() && bulbs[ idx ] ) { // ok current bulb we point is turned on now, 
      ++cnt; // that's the moment of turning on lights
      // now, move idx to first tuned off
      while ( idx < seq.size() && bulbs[ idx ] ) {
        std::cout << "Turning on: " << idx << std::endl;
        ++idx;
      }
    }
  }
  return cnt;
}

template < typename T >
std::ostream & operator << ( std::ostream & s, std::vector< T > const & v ) {
  s << "[";
  for ( auto i{ v.begin()}; i != v.end(); ++i ) {
    s << " " << *i;
  }
  s << " ]";
  return s;
}

int main() {
  {
    auto const seq{ std::vector< int >{ 0, 1, 2, 3 } };
    std::cout << std::endl << ">> Test seq: " << seq << std::endl;
    auto const moments{ countMoments( seq ) };
    assert( moments == 4 );
  }

  {
    auto const seq{ std::vector< int > { 3, 2, 1, 0 } };
    std::cout << std::endl << ">> Test seq: " << seq << std::endl;
    auto const moments{ countMoments( seq ) };
    assert( moments == 1 );
  }
}
