class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        auto const N{ nums.size() };
        for ( auto i{ 0u }; i < N; ++i ) {
            while ( nums[ i ] > 0 && nums[ i ] < N + 1 && nums[ nums[ i ] - 1 ] != nums[ i ] ) {
                swap( nums[ i ], nums[ nums[ i ] - 1 ] );
            }
        }
        for ( auto i{ 0u }; i < N; ++i ) {
            if ( nums[ i ] != i+1 ) {
                return i+1;
            }
        }
        return N+1;
    }
    
    Solution() {
    }
    
};
