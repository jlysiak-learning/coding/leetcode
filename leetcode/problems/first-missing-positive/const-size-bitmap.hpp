class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        for ( auto n : nums ) {
            if ( n > 0 && n <= 500000 ) {
                markPresent( n );
            }
        } 
        return firstMissing();
    }
    
    Solution() {
        bits.resize( size, 0 );
    }
    
private:
    constexpr static size_t size{ (500001 + 31) / 32 };
    vector< uint32_t > bits;
    
    void markPresent( size_t idx ) {
        auto const byteIdx{ idx / 32u };
        auto const bitIdx{ idx & 0x1f };
        bits[ byteIdx ] |= 0x1 << bitIdx;
    }
    
    int firstMissing() {
        for ( size_t i{ 1 }; i <= 500000; ++i ) {
            auto const idx{ i / 32u };
            auto const bitIdx{ i & 0x1f };
            if ( !( bits[ idx ] & ( 0x1 << bitIdx ) ) ) {
                return i;
            }
        }
        return 500001;
    }
};
