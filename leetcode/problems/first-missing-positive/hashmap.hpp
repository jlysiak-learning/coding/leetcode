class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        auto const N{ nums.size() };
        for ( int i = 0; i < N; ++i ) {
            if ( nums[ i ] < 0 || nums[ i ] > N ) {
                nums[ i ] = 0;
            }
        }
        for ( int i = 0; i < N; ++i ) {
            auto const idx{ ( nums[ i ] % static_cast<int>(N + 1) ) - 1 };
            if ( idx < 0 ) {
                continue;
            }
            nums[ idx ] += N + 1;
        }
        for ( int i = 0; i < N; ++i ) {
            auto const val{ nums[ i ] };
            auto const cnt{ val / static_cast<int>(( N + 1 )) };
            if ( !cnt ) {
                return i+1;
            }
        }
        return N+1;
    }
    
    Solution() {
    }
    
};
