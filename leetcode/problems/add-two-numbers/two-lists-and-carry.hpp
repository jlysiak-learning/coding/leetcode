/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        auto const carry{ bool{} };
        return add( l1, l2, carry );
    }
    
private:
    ListNode * add( ListNode * d1, ListNode * d2, bool carry ) {
        if ( !d1 && !d2 && carry ) {
            return new ListNode{ 1, nullptr };
        }
        if ( !d1 && !d2 ) {
            return nullptr;
        }
        
        auto sum{ carry ? 1 : 0 };
        if ( d1 ) {
            sum += d1->val;
        }
        if ( d2 ) {
            sum += d2->val;
        }
        auto const val{ sum % 10 };
        auto const carryOver{ sum >= 10 };
        auto node{ new ListNode{ val } };
        node->next = add( d1 ? d1->next : nullptr,
                          d2 ? d2->next : nullptr,
                          carryOver );
        return node;
    }
};
