/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

class Solution {
public:
    bool hasCycle(ListNode *head) {
        auto t{ head };
        auto h{ head };
        do {
            if ( h == nullptr ) {
                return false;
            }
            h = h->next;
            if ( h == nullptr ) {
                return false;
            }
            h = h->next;
            t = t->next; 
        } while ( t != h );
        
        return true;
    }
};
