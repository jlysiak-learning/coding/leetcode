class Solution {
private:
    constexpr static auto NOTVISITED{ -1 };
    int preCnt{ 0 };
    int postCnt{ 0 };
    vector< decltype( preCnt ) > pre;
    vector< decltype( preCnt ) > post;
    vector< vector< int > > adj;
    bool cycle{ bool{} };
    
public:
    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
        auto const n{ numCourses };
        pre.resize( n, NOTVISITED );
        post.resize( n, NOTVISITED );
        adj.resize( n );
        for ( auto i{ size_t{} }; i < prerequisites.size(); ++i ) {
            auto const a{ prerequisites[ i ][ 0 ] };
            auto const b{ prerequisites[ i ][ 1 ] };
            adj[ a ].push_back( b );
        }
        for ( auto i{ size_t{} }; i < n; ++i ) {
            if ( pre[ i ] != NOTVISITED ) {
                continue;
            } 
            dfs( i );
        }
        return !cycle;
    }
    
    void dfs( int n ) {
        pre[ n ] = preCnt++;
        for ( auto v : adj[ n ] ) {
            if ( pre[ v ] == NOTVISITED ) {
                dfs( v ); 
            } else if ( post[ v ] == NOTVISITED ) {
               // not processed but already visited, cycle
                cycle = true;
            }
        } 
        post[ n ] = postCnt++;
    }
};
