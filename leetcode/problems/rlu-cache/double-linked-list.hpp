class LRUCache {
    
private:
    class Node {
    public:
        int key{-1};
        int val{-1};
        
        Node * prev{ nullptr };
        Node * next{ nullptr };
    };
    
    Node * head_{ nullptr };
    Node * tail_{ nullptr };
    vector< Node * > nodes_;
    
public:
    LRUCache(int capacity) {
        assert( capacity > 0 );
        
        head_ = new Node; 
        head_->prev = head_;
        
        auto prev{ head_ };
        for ( int i{ 1 }; i < capacity; ++i ) {
            auto node{ new Node };
            prev->next = node;
            node->prev = prev;
            prev = node;
        }
        tail_ = prev;
        tail_->next = tail_;
        
        nodes_.resize( 10001, nullptr );
    }
    
    int get( int key ) {
        auto node{ getNode( key ) };
        if ( node == nullptr ) {
            return -1;
        }
        rotate( head_, node );
        return node->val;
    }
    
    void put( int key, int value ) {
        auto node{ getNode( key ) };
        if ( node == nullptr ) {
            // not found, rotate and move LRU node to head to be overwritten
            if ( tail_->key != -1 ) {
                nodes_[ tail_->key ] = nullptr;
            }
            rotate( head_, tail_ );
            node = head_;
        }
        node->key = key;
        node->val = value;
        nodes_[ key ] = node;
        rotate( head_, node );
    }

private:
    Node * getNode( int key ) {
        assert( key >= 0 && key <= 10000 );
        return nodes_[ key ];
    }
    
    void rotate( Node * b, Node * e ) {
        if ( b == e ) {
            return;
        }
        
        if ( e == tail_ ) {
            e->prev->next = e->prev;
            tail_ = e->prev;
        } else {
            e->prev->next = e->next;
            e->next->prev = e->prev;
        }
        if ( b == head_ ) {
            b->prev = e;
            e->prev = e;
            e->next = b;
            head_ = e;
        } else {
            b->prev->next = e;
            e->prev = b->prev;
            b->prev = e;
            e->next = b;
        }
    }
};
