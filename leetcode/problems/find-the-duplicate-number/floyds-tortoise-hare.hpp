// Stats reported by Leetcode:
// Runtime: 124 ms
// Memory Usage: 61.1 MB

#pragma once

class Solution {
  public:
    int findDuplicate( vector<int> &nums ) {
        auto t { 0 };
        auto h { 0 };
        do {
            t = nums [ t ];
            h = nums [ nums [ h ] ];
        } while ( t != h );

        t = 0;
        do {
            t = nums [ t ];
            h = nums [ h ];
        } while ( t != h );
        return t;
    }
};
