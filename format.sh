#!/bin/sh

find . -type f -name "*.hpp" | xargs clang-format -i
